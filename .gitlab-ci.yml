image: registry.gitlab.com/morpheus/website-builder/hugo_extended:0.89.4

workflow: # Switch between branch pipelines when a merge request is not open for the branch and merge request pipelines when a merge request is open for the branch; https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
  rules:
#    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS # Run jobs for every push, but only branch type pipelines if no MR is created, and MR type pipelines when the MR is created, but do not block triggered branch pipelines; https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines;
#      when: never
#    - when: always # https://gitlab.com/gitlab-org/gitlab/-/issues/29508#note_489444538
#    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH' # Netlify always inserts its external job into branch pipelines, which leads to multiple pipelines running when MR is open (branch + MR pipeline). This causes the Netlify branch pipeline in a MR to take precedence over the actually important MR pipeline and success or failure is only determined based on the Netlify build and not the GitLab build jobs (the latter would be correct). So on the GitLab side only use branch pipelines and no MR pipelines for now.

stages:
  - Build Image
  - Test Model Repo
  - Test Hugo Site
  - Build & Deploy

build_hugo_image:
  stage: Build Image
  trigger:
    project: morpheus/website-builder
    branch: master
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline" # Run in multi-project pipelines only (i.e. trigger when working on 'website' or 'model-repo-parser', not on 'model-repo')
  when: manual
  allow_failure: true

validate_models:
  stage: Test Model Repo
  environment:
    name: testing
  before_script:
    - cd ../ # Model parser requires the pure model repo, so put the Hugo page somewhere else
    - git clone https://gitlab+deploy-token-morpheus-website-read-access:_89WXDU_Cz4cbzdDMhUr@gitlab.com/morpheus/morpheus.gitlab.io.git morpheus-website # https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html; Security note: GitLab Deploy token is here in plain text on purpose and read access to website repository is intended in order to enable successful completion of model repo CI pipelines in contributor forks
    - cd morpheus-website/ && git submodule update --init --recursive
    - cd models/parser/ && git checkout master && git checkout $CI_COMMIT_REF_NAME_UPSTREAM && cd ../../../ # Try to checkout upstream branch/tag; if the variable is not set (i. e. this pipeline was not triggered by an upstream pipeline) 'git checkout' has no effect (Git manual: 'You could omit <branch>, in which case the command degenerates to "check out the current branch" which is a glorified no-op with rather expensive side-effects to show only the tracking information, if exists, for the current branch.')
    - pip install --upgrade pip && pip install -r morpheus-website/models/parser/requirements.txt
  script:
    - python3 morpheus-website/models/parser/models.py build --validate --stats --repo-dir ../../../$CI_PROJECT_NAME/
    - mv morpheus-website/ $CI_PROJECT_NAME/ # Artifacts have to be inside build directory
  rules: # Do not mix only/except jobs with rules jobs in the same pipeline (jobs with no rules default to except: merge_requests); https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH'
  artifacts:
    paths: # Path relative to the build directory; https://gitlab.com/gitlab-org/gitlab-foss/-/issues/15530#note_5049321
      - morpheus-website/
    exclude:
      - morpheus-website/models/**/*
    expire_in: 20 minutes

build_hugo_site:
  stage: Test Hugo Site
  environment:
    name: testing
  script:
    - cd morpheus-website/
    - hugo --gc --minify --buildFuture
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH'
  dependencies:
    - validate_models
  artifacts:
    paths:
      - morpheus-website/
    exclude:
      - morpheus-website/models/**/*
      - morpheus-website/public/
      - morpheus-website/resources/
    when: on_failure # Upload artifacts on failure to help debug parser and hugo output (https://docs.gitlab.com/ee/ci/yaml/index.html#artifactswhen)
  allow_failure:
    exit_codes: 413 # 'Payload Too Large': Maximum size of job artifacts could exceed the GitLab.com limit of 1 GB (compressed) and raise an error (https://docs.gitlab.com/ee/ci/yaml/#allow_failureexit_codes, https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlab-cicd)

bridge:
  stage: Build & Deploy
  rules:
    - if: ($CI_PIPELINE_SOURCE != "pipeline" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH) || ($CI_PIPELINE_SOURCE == "pipeline" && $CI_COMMIT_REF_NAME_UPSTREAM == $CI_DEFAULT_BRANCH_UPSTREAM) # If pipeline is not a triggered multi-project pipeline on the default branch or pipeline is a downstream multi-project pipeline triggered by an upstream pipeline's default branch, deploy; https://docs.gitlab.com/ee/ci/triggers/index.html#configure-cicd-jobs-to-run-in-triggered-pipelines
  trigger:
    project: morpheus/morpheus.gitlab.io
    branch: master
