---
MorpheusModelID: M7122

title: "MDCK EMT B"

authors: [N. Mukhtar, E. N. Cytrynbaum, L. Edelstein-Keshet]
contributors: [E. N. Cytrynbaum, Y. Xiao]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1016/j.bpj.2022.04.010"
  title: "A multiscale computational model of YAP signaling in epithelial fingering behavior"
  journal: "Biophys. J."
  volume: 121
  issue: 10
  page: 1940-1948
  year: 2022
  original_model: true

categories:
- DOI:10.1016/j.bpj.2022.04.010

tags:
- 2D
- Adherent Slow Cells
- Cancer Metastasis
- Cell-Cell Adhesion
- Cell Migration
- Cellular Potts Model
- Cell Speed
- CPM
- E-cadherin
- Embryonic Development
- EMT
- Epithelial Fingering
- Epithelial-Mesenchymal Transition
- Epithelial Sheet
- Epithelium
- Finger-like Projection
- GTPase
- Intracellular Signaling
- Knockdown
- Mechanochemical Control
- Mesenchyme
- Motile Loose Cells
- Multiscale Model
- ODE
- Ordinary Differential Equation
- Overexpression
- Rac1
- Single Cell Model
- Topographic Cues
- Wound Healing
- YAP
- YAP1
- YAP65
- Yes-associated Protein
---
> Simulated sheet morphology for YAP knockdown (KD) and overexpression (OE)

## Introduction

We model epithelial-mesenchymal transition (EMT) by first assembling an ODE model for intracellular Yes-associated protein (YAP) signalling and then embedding this single cell model within individual cells in a multiscale simulation.

Knockdown (KD) and overexpression (OE) experiments in [Park *et al.* (2019)][Park2019] were represented by adjusting the total YAP level.

## Description

We simulate YAP KD by adjusting the total YAP level, namely $`Y_\text{tot}= 0.5`$. YAP OE is simulated by setting $`Y_\text{tot} = 2.0`$.

## Results

In the YAP KD simulation, the sheets expand slowly, with very low active YAP throughout. In YAP OE, multiple fingers form and grow early as cells rapidly jump to the high-YAP state. These observations are consistent with experiments in [Park *et al.* (2019)][Park2019], although our fingers are wider in the YAP KD case.

![](NRA-Y-KD_main.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m7122/NRA-Y-KD_main.xml" >}}:** Simulated YAP KD sheet morphology with $`Y_\text{tot} = 0.5`$. Fingers take longer to form than in the control case, and active YAP is low throughout the sheet.

![](NRA-Y-OE.mp4) 
**Video of the model simulation {{< model_quick_access "media/model/m7122/NRA-Y-OE.xml" >}}:** Simulated YAP OE sheet morphology with $`Y_\text{tot} = 2`$. Note the widespread high active YAP concentrations and aggressive finger growth.

[Park2019]: https://doi.org/10.1038/s41467-019-10729-5