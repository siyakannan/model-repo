<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

Produced SI Figure 11

We thank Lutz Brusch for providing a basic cell sheet simulation that we modified and adapted to this project </Details>
        <Title>YREsheetNRAShapeFactorq</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size value="400, 100, 0" symbol="size"/>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="noflux" boundary="-x"/>
                <Condition type="periodic" boundary="y"/>
                <Condition type="periodic" boundary="-y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1500" symbol="stoptime"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Analysis>
        <Gnuplotter decorate="true" time-step="50">
            <Terminal name="png"/>
            <Plot>
                <Cells flooding="true" max="5" min="3" value="q">
                    <Disabled>
                        <ColorMap>
                            <Color color="lemonchiffon" value="0"/>
                            <Color color="light-blue" value="0.05"/>
                            <Color color="light-red" value="0.1"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <!--    <Disabled>
        <Plot title="Rac1">
            <Cells value="R" min="0" max="3" flooding="true">
                <ColorMap>
                    <Color value="0" color="blue"/>
                    <Color value="3" color="red"/>
                    <Color value="4" color="yellow"/>
                </ColorMap>
            </Cells>
        </Plot>
    </Disabled>
-->
            <!--    <Disabled>
        <Plot>
            <Cells value="E" min="0" max="2" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="plum"/>
                        <Color value="4" color="blue"/>
                        <Color value="7" color="cyan"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
            <!--    <Disabled>
        <Plot>
            <Cells value="d.abs" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="skyblue"/>
                        <Color value="0.1" color="violet"/>
                        <Color value="0.2" color="salmon"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
            <!--    <Disabled>
        <Plot>
            <Cells value="Cr" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="red"/>
                        <Color value="0" color="red"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
        </Gnuplotter>
        <Logger time-step="1.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="q"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="-1">
                    <Style point-size="0.05" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis maximum="size.x" minimum="0.0">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis maximum="stoptime" minimum="0">
                        <Symbol symbol-ref="time"/>
                    </Y-axis>
                    <Color-bar maximum="5" minimum="3" palette="default">
                        <Symbol symbol-ref="q"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
    <Global>
        <Constant value="0.1" name="Basal rate of YAP activation" symbol="ky"/>
        <Constant value="1.8" name="E-cadherin-dependent rate of YAP deactivation" symbol="kye"/>
        <Constant value="2" name="Inactivation rate of YAP" symbol="Dy"/>
        <Constant value="0.9" name="Initial activation rate of E-cadherin" symbol="C"/>
        <Constant value="0.9" name="YAP-dependent rate of E-cadherin expression" symbol="ke"/>
        <Constant value="1" name="Dissociation constant of YAP-WT1 transcriptional constant" symbol="K"/>
        <Constant value="1" name="Inactivation rate of E-cadherin" symbol="De"/>
        <Constant value="3" name="Hill coefficient for E-cadherin" symbol="h"/>
        <Constant value="1" name="YAP-dependent rate of Rac1 expression" symbol="kr"/>
        <Constant value="0.5" name="Michaelis-Menten-like constant for Rac1" symbol="Kr"/>
        <Constant value="0.5" name="Degradation rate of Rac1" symbol="Dr"/>
        <Constant value="6" name="Hill coefficient for Rac1" symbol="n"/>
        <Constant value="1" name="Rac activation fraction" symbol="alphaR"/>
        <Constant value="1.8" name="Rac1-dependent rate of YAP activation" symbol="kyr"/>
        <!--    <Disabled>
        <Constant symbol="A2" name="basal adhesion" value="2">
            <Annotation>E-cad blocking</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="12" name="Max E-cadherin adhesion constant" symbol="A2">
            <Annotation>Control</Annotation>
        </Constant>
        <Constant value="0.85" name="E-cadherin half &quot;saturation&quot; " symbol="A3"/>
        <Constant value="0.4" name="basal migration" symbol="C1"/>
        <Constant value="4" name="Max Rac1 migration constant" symbol="C2"/>
        <Constant value="3" name="Rac1 half &quot;saturation&quot;" symbol="C3"/>
        <Constant value="100" name="max time for initial leader cells to emerge" symbol="tlim"/>
        <Constant value="0.2" name="fraction of neighbourhood Cr that the cell receives each time Cr spreads to it" symbol="frac"/>
        <!--    <Disabled>
        <Constant symbol="Ytot" name="Total YAP (active (Y) + inactive)" value="0.5">
            <Annotation>KD</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="1.2" name="Total YAP (active (Y) + inactive)" symbol="Ytot">
            <Annotation>Control</Annotation>
        </Constant>
        <!--    <Disabled>
        <Constant symbol="Ytot" name="Total YAP (active (Y) + inactive)" value="2">
            <Annotation>OE</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant value="5" name="Total Rac1 (active (R) + inactive)" symbol="Rtot"/>
        <Constant value="0.02" name="parameter that determines probability of Cr spread in each time step" symbol="shareprob"/>
        <Variable value="0.0" name="E-cadherin" symbol="E"/>
        <Variable value="0.0" name="Basal Rac1 activation rate" symbol="Cr"/>
        <!--    <Disabled>
        <Variable symbol="x_edge2" value="0.0"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Mapper name="leftmost edge cell" time-step="1.0">
            <Input value="(M>0)*cell.center.x*(cell.center.x>10) + (M==0)*size.x + (M>0)*size.x*(cell.center.x&lt;=10)"/>
            <Output symbol-ref="x_edge2" mapping="minimum"/>
        </Mapper>
    </Disabled>
-->
    </Global>
    <CellTypes>
        <CellType name="medium" class="medium"/>
        <CellType name="dividingcell" class="biological">
            <VolumeConstraint strength="1" target="50"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint strength="1" target="1" mode="aspherity"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;40) * (cell.volume>40)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion strength="C1+C2*R/(C3+R)" direction="1, 0.0, 0.0" name="cell migration"/>
            <Property value="0" name="YAP" symbol="Y"/>
            <Property value="10" name="E-cadherin" symbol="E"/>
            <Property value="0" name="Rac1" symbol="R"/>
            <Property value="0.001" symbol="Cr"/>
            <Property value="0.0" name="distance from right domain edge" symbol="dist"/>
            <Property value="0.0" name="Sum of instantaneous speeds each time step (times 100)" symbol="avspeed"/>
            <Property value="0.0" name="Average Speed * 100" symbol="truavspeed"/>
            <Property value="0.0" name="Sum of instantaneous speeds over final 200 time steps (times 100)" symbol="avspeed2"/>
            <Property value="0.0" name="average speed over final 200 time steps (times 100)" symbol="truavspeed2"/>
            <Property value="0.0" name="Contact with medium" symbol="M"/>
            <Property value="0.0" name="number of neighbour cells" symbol="neigh"/>
            <Property value="0.0" name="average neighbourhood Cr" symbol="av"/>
            <PropertyVector value="0.0, 0.0, 0.0" name="speed" symbol="d"/>
            <NeighborhoodReporter>
                <Input scaling="length" value="cell.type == celltype.medium.id"/>
                <Output mapping="sum" symbol-ref="M"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.dividingcell.id"/>
                <Output mapping="sum" symbol-ref="neigh"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="Cr"/>
                <Output mapping="average" symbol-ref="av"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y" name="Equation for YAP">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E" name="Equation for E-cadherin">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R" name="Equation for Rac1">
                    <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <Rule symbol-ref="Cr" name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr">
                    <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
                </Rule>
                <Rule symbol-ref="dist" name="distance from right domain edge">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule symbol-ref="avspeed" name="Sum of instantaneous speeds each time step (times 100)">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed" name="average speed over the simulation (times 100)">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule symbol-ref="avspeed2" name="Sum of instantaneous speeds over final 100 time steps (times 100)">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed2" name="average speed over 100 time steps (times 100)">
                    <Expression>avspeed2/200</Expression>
                </Rule>
                <Rule symbol-ref="q">
                    <Expression>cell.surface/sqrt(cell.volume)</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="q">
            <Expression>cell.volume</Expression>
        </Rule>
    </Disabled>
-->
            </System>
            <Property value="0.0" name="shape index" symbol="q"/>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population type="dividingcell" name="Initialize cell sheet at left edge of the domain" size="1">
            <!--    <Disabled>
        <InitRectangle number-of-cells="70" mode="regular">
            <Dimensions size="size.x/100, size.y, size.z" origin="size.x/100, 0, 0"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle number-of-cells="70" mode="regular">
                <Dimensions origin="0.0, 0.0, 0.0" size="4.0, size.y, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact type1="dividingcell" type2="dividingcell" value="30">
                <AddonAdhesion strength="5" name="Adhesive strength" adhesive="A2*E/(A3+E)"/>
            </Contact>
            <Contact type1="dividingcell" type2="medium" value="12"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
