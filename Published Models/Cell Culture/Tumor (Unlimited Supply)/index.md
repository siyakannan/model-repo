---
MorpheusModelID: M0007

authors: [N. Jagiella, D. Rickert, F. J. Theis, J. Hasenauer]
contributors: [R. Müller, L. Brusch, E. Alamoudi, J. Hasenauer]

title: "Tumor Growth: Unlimited Oxygen and Glucose Supply"

# Reference details
publication:
  doi: "10.1016/j.cels.2016.12.002"
  title: "Parallelization and High-Performance Computing Enables Automated Statistical Inference of Multi-scale Models"
  journal: "Cell Systems"
  volume: 4
  issue: 2
  page: "194-206.e9"
  year: 2017
  original_model: false

tags:
- 2D
- 3D
- ABC SMC Algorithm
- Approximate Bayesian Computation
- Approximate Bayesian Computation Sequential Monte Carlo Algorithm
- Bayesian Parameter Estimation
- Benchmark
- Growth Curve
- High-Performance Computing
- Histological Data
- HPC
- Hybrid Deterministic-Stochastic Behavior
- Mechanistic Model
- Media Droplets
- Medium
- Model-Based Data Integration
- Monte Carlo
- Monte Carlo Algorithm
- Multi-Scale Model
- Nutrient Environment
- pABC SMC Algorithm
- Parallel Approximate Bayesian Computation Sequential Monte Carlo Algorithm
- Parallelization
- Parameter Estimation
- Statistical Inference
- Tumor
- Tumor Spheroid
- Tumor Spheroid Growth

categories:
- DOI:10.1016/j.cels.2016.12.002

# order
#weight: 10
---
## Introduction

In [Jagiella et al. 2017](#reference) a quantitative single cell-based mathematical model for multi-cellular tumor
spheroids was parametrized by Bayesian inference for different scenarios of nutrition and oxygen supply. The simulations generate of the order of a million individual cells. Experimental data (growth kinetics, spatial patterns for cell proliferation, cell death, extracellular matrix density) were taken from a non-small cell lung cancer (NSCLC) cell line in earlier work [Jagiella et al. 2016](https://doi.org/10.1371/journal.pcbi.1004412). The **original** open-source model is available on [GitHub](https://github.com/ICB-DCM/pABC-SMC).

![](fig-4a.png "[Fig. 4a](https://www.cell.com/cell-systems/fulltext/S2405-4712(16)30412-4#fig4) shows the original model results (blue, and intermediate fitting states in grey) in comparison to the experimental data (green symbols, at day 17 for ECM and proliferation ratio pattern). ([CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/): [Jagiella et al. 2017](#reference))")

## Description
Here, the Morpheus model quantitatively reproduces the published results for scenario I of unlimited oxygen and nutrient supply. Thus, the model focuses on cell mechanics, motility, proliferation and the secretion, turnover of extracellular matrix (ECM). 

[Jagiella et al. 2017](#reference) had modelled cell migration and spatial competition implicitly for point-like agents in a cellular automaton on a network, including long-range cell displacements to make room for cell divisions. In the Morpheus model, the Cellular Potts Model (CPM) formalism is used to represent moving and growing cells with spatial resolution and explicit, purely local competition for space. While most parameter values were maintained as published, to account for the different cell mechanics model, the proliferation rate `k_div_max` had to be re-adjusted. In addition, the observables ECM density and proliferation ratio as well as the initial radius of the spheroid `R_init` were rescaled by constant factors to match the experimental units.

Below, the proliferation ratio histogram and comparative charts between simulation results and experimental data were created during simulation by the Morpheus plugin `External`. This plugin enables live data processing and visualization (beyong Morpheus' on-board capabilities) during the simulation by starting arbitrary external programs (here: awk scripts and gnuplot).

- **Units** in the model are $\[\text{space}\] = \mathrm{\mu m}$, \[time\] = hours respectively days.
- **Parameter values** were taken from [Jagiella et al. 2017](#reference).
- **Boundary conditions** are no-flux along the domain boundary by default. The model is here simulated in 2D and is ready for 3D.
- **Initial condition**: a mix of proliferating and quiescent cells are placed in a circle with initial radius `R_init` at the center of the lattice. 

## Results
   
This movie of a Morpheus model simulation shows proliferating cells in red and quiescent cells in yellow, in the left panel. In the right panel, ECM density is color coded as indicated. Time in hours is given at the bottom.

![](movie.mp4)

The results for scenario I with abundant nutrients and oxygen from [Jagiella et al. 2017](#reference) are quantitatively reproduced after rescaling of the observables. 
For illustration purposes we only show results from a single simulation run while the published plots show ensemble data of many runs.

![](growthcmp.png "Growth curve of tumor spheroid radius obtainied with the Morpheus model (line) vs. experimental data (symbols), matching the upper-left panel in Fig.4a above.")
![](prolifcmpday17.png "Proliferation ratio profile at day 17 obtained with the Morpheus model (line) vs. experimental data (symbols), matching the upper-middle panel in Fig.4a above.")
![](ecmcmpday17.png "ECM density profile at day 17 obtained with the Morpheus model vs. experimental data (symbols), matching the upper-right panel in Fig.4a above.")

{{% callout note %}}
Note that the first day in this CPM dynamics is overshadowed (cf. radius curve) by the cell shape relaxation that is initialized simply with point-like cells. Alternative initializations with larger cells are of course possible.
{{% /callout %}}
