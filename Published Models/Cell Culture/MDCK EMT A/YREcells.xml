<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

This file produced SI Figure 3.

A few cells with the YAP-Rac-E cadherin circuit governing their motility and adhesion. Model based on 


Park, J., Kim, D.H., Shah, S.R., Kim, H.N., Kim, P., Quiñones-Hinojosa, A. and Levchenko, A., 2019. Switch-like enhancement of epithelial-mesenchymal transition by YAP through feedback regulation of WT1 and Rho-family GTPases. Nature communications, 10(1), pp.1-15.
</Details>
        <Title>YREcells</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size value="200, 100, 0" symbol="size"/>
            <BoundaryConditions>
                <Condition boundary="x" type="constant"/>
                <Condition boundary="-x" type="noflux"/>
                <Condition boundary="y" type="periodic"/>
                <Condition boundary="-y" type="periodic"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="1000" symbol="stoptime"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="27"/>
    </Time>
    <Analysis>
        <Gnuplotter time-step="50" decorate="true">
            <Terminal name="png"/>
            <Plot>
                <Cells value="Y" flooding="true"/>
                <CellArrows style="1" orientation="3 * d / d.abs"/>
            </Plot>
            <Plot>
                <!--    <Disabled>
        <Cells value="d.abs" flooding="true">
            <Disabled>
                <ColorMap>
                    <Color value="0" color="skyblue"/>
                    <Color value="0.1" color="violet"/>
                    <Color value="0.2" color="salmon"/>
                </ColorMap>
            </Disabled>
        </Cells>
    </Disabled>
-->
            </Plot>
            <Plot>
                <Cells value="truavspeed" flooding="true">
                    <!--    <Disabled>
        <ColorMap>
            <Color value="0" color="blue"/>
            <Color value="1" color="green"/>
        </ColorMap>
    </Disabled>
-->
                </Cells>
            </Plot>
            <!--    <Disabled>
        <Plot>
            <Cells value="Cr" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="lemonchiffon"/>
                        <Color value="0.05" color="light-blue"/>
                        <Color value="1" color="light-red"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
        </Gnuplotter>
        <ModelGraph reduced="false" include-tags="#untagged" format="svg"/>
    </Analysis>
    <Global>
        <Field value="space.x/size.x" name="wound chemoattractant" symbol="c"/>
        <Constant value="0.1" name="Basal rate of YAP activation" symbol="ky"/>
        <Constant value="1.8" name="E-cadherin-dependent rate of YAP deactivation" symbol="kye"/>
        <Constant value="2" name="Inactivation rate of YAP" symbol="Dy"/>
        <Constant value="0.9" name="Initial activation rate of E-cadherin" symbol="C"/>
        <Constant value="0.9" name="YAP-dependent rate of E-cadherin expression" symbol="ke"/>
        <Constant value="1" name="Dissociation constant of YAP-WT1 transcriptional constant" symbol="K"/>
        <Constant value="1" name="Inactivation rate of E-cadherin" symbol="De"/>
        <Constant value="3" name="Hill coefficient for E-cadherin" symbol="h"/>
        <!--    <Disabled>
        <Constant value="0" name="Initial activation rate of Rac1" symbol="Cr"/>
    </Disabled>
-->
        <Constant value="1" name="YAP-dependent rate of Rac1 expression" symbol="kr"/>
        <Constant value="0.5" name="Michaelis-Menten-like constant for Rac1" symbol="Kr"/>
        <Constant value="0.5" name="Degradation rate of Rac1" symbol="Dr"/>
        <Constant value="6" name="Hill coefficient for Rac1" symbol="n"/>
        <Constant value="1.8" name="Rac1-dependent rate of YAP activation" symbol="kyr"/>
        <Constant value="2" name="basal adhesion" symbol="A1"/>
        <Constant value="1" name="Max E-cadherin adhesion constant" symbol="A2"/>
        <Constant value="0.85" name="E-cadherin half &quot;saturation&quot; " symbol="A3"/>
        <Constant value="2" name="basal migration" symbol="C1"/>
        <Constant value="5" name="Max Rac1 migration constant" symbol="C2"/>
        <Constant value="3" name="Rac1 half &quot;saturation&quot;" symbol="C3"/>
        <!--    <Disabled>
        <Variable value="0" name="Rac1-dependent rate of YAP activation" symbol="kyr"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Variable value="0.0" symbol="ky"/>
    </Disabled>
-->
        <Constant value="2" name="parameter for total YAP (active (Y) + inactive)" symbol="Ytot"/>
        <Constant value="5" symbol="Rtot"/>
        <Variable value="0.0" symbol="E"/>
        <Variable value="0.0" symbol="Cr"/>
    </Global>
    <CellTypes>
        <CellType class="medium" name="medium"/>
        <!--    <Disabled>
        <CellType class="biological" name="addedcell">
            <Chemotaxis field="c" contact-inhibition="true" strength="100*(C1+C2*(R>C3))"/>
            <VolumeConstraint target="50" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint target="1" strength="1" mode="aspherity"/>
            <Property value="0" name="YAP" symbol="Y"/>
            <Property value="max(2.0+0.1*rand_norm(0.9,0.5),0)" name="E-cadherin" symbol="E"/>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y">
                    <Expression> epsilon*(ky - kye*Y*E - Dy*Y +kyr*R)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E">
                    <Expression> epsilon*(C - ke*Y^h/(K^h+Y^h) - De*E)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R">
                    <Expression> epsilon*(Cr + kr*Y^n/(Kr^n+Y^n) - Dr*R)</Expression>
                </DiffEqn>
                <Rule symbol-ref="kyr">
                    <Expression>kyr+(3*(M>10)*(rand_uni(0,1)&lt;0.008)+0.03*av^0.3*(rand_uni(0,1)&lt;0.03))*(kyr&lt;3)*(cell.center.x>size.x/10)</Expression>
                </Rule>
                <Rule symbol-ref="av">
                    <Expression>(neigh>0)*(su/max(neigh,1))</Expression>
                </Rule>
            </System>
            <Property value="0" symbol="R"/>
            <PersistentMotion strength="0.15" decay-time="20"/>
            <DirectedMotion strength="0.1" direction="1, 0.0, 0.0"/>
            <PropertyVector value="0.0, 0.0, 0.0" symbol="d"/>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <Property value="0" symbol="kyr"/>
            <Property value="0.0" symbol="su"/>
            <NeighborhoodReporter>
                <Input value="kyr" scaling="cell"/>
                <Output mapping="sum" symbol-ref="su"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output mapping="sum" symbol-ref="M"/>
            </NeighborhoodReporter>
            <Property value="0.0" symbol="M"/>
            <Property value="0.0" symbol="W"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.wall.id" scaling="cell"/>
                <Output mapping="sum" symbol-ref="W"/>
            </NeighborhoodReporter>
            <Property value="0.0" symbol="av"/>
            <Property value="0.0" symbol="neigh"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.addedcell.id" scaling="cell"/>
                <Output mapping="sum" symbol-ref="neigh"/>
            </NeighborhoodReporter>
            <AddCell>
                <Distribution>space.x &lt; 0.1*size.x</Distribution>
                <Triggers/>
                <Count>0.0</Count>
            </AddCell>
        </CellType>
    </Disabled>
-->
        <!--    <Disabled>
        <CellType class="biological" name="leadercell">
            <Chemotaxis field="c" contact-inhibition="true" strength="1000*(C1+C2*(R>C3))"/>
            <VolumeConstraint target="50" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint target="1" strength="1" mode="aspherity"/>
            <Disabled>
                <CellDivision division-plane="major">
                    <Condition>(rand_uni(0,1)&lt;0.008*(exp(-time/300)+0.2)) * (cell.center.x&lt;size.x*0.1)</Condition>
                    <Triggers>
                        <Rule symbol-ref="Y">
                            <Expression>max(0.7 + 0.3*rand_norm(2,3),0)</Expression>
                        </Rule>
                        <Rule symbol-ref="E">
                            <Expression>max(0.1+rand_norm(0.9,0.5),0)</Expression>
                        </Rule>
                        <Rule symbol-ref="R">
                            <Expression>max(0.35+rand_norm(0.1,0.1),0)</Expression>
                        </Rule>
                    </Triggers>
                </CellDivision>
            </Disabled>
            <Property value="0" name="YAP" symbol="Y"/>
            <Property value="max(2.0+0.1*rand_norm(0.9,0.5),0)" name="E-cadherin" symbol="E"/>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y">
                    <Expression> epsilon*(ky - kye*Y*E - Dy*Y +kyr*R)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E">
                    <Expression> epsilon*(C - ke*Y^h/(K^h+Y^h) - De*E)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R">
                    <Expression> epsilon*(Cr + kr*Y^n/(Kr^n+Y^n) - Dr*R)</Expression>
                </DiffEqn>
                <Disabled>
                    <Rule symbol-ref="kyr">
                        <Expression>kyr+(10*(M>10)*(rand_uni(0,1)&lt;0.008)+0.05*av^0.3*(rand_uni(0,1)&lt;0.03))*(kyr&lt;10)*(cell.center.x>size.x/10)</Expression>
                    </Rule>
                </Disabled>
                <Rule symbol-ref="av">
                    <Expression>(neigh>0)*(su/max(neigh,1))</Expression>
                </Rule>
                <Disabled>
                    <Rule symbol-ref="ky">
                        <Expression>1+2*((cell.center.y>0.125*size.y)*(cell.center.y&lt;0.25*size.y) + (cell.center.y>0.375*size.y)*(cell.center.y&lt;0.5*size.y))</Expression>
                    </Rule>
                </Disabled>
                <Rule symbol-ref="avspeed">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed">
                    <Expression>avspeed/time</Expression>
                </Rule>
            </System>
            <Property value="0" symbol="R"/>
            <Disabled>
                <PersistentMotion strength="0.15" decay-time="20"/>
            </Disabled>
            <DirectedMotion strength="0.1" direction="1, 0.0, 0.0"/>
            <PropertyVector value="0.0, 0.0, 0.0" name="Speed" symbol="d"/>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <Property value="10" symbol="kyr"/>
            <Property value="0.0" symbol="su"/>
            <NeighborhoodReporter>
                <Input value="kyr" scaling="cell"/>
                <Output mapping="sum" symbol-ref="su"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output mapping="sum" symbol-ref="M"/>
            </NeighborhoodReporter>
            <Property value="0.0" symbol="M"/>
            <Property value="0.0" symbol="av"/>
            <Property value="0.0" symbol="neigh"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.dividingcell.id" scaling="cell"/>
                <Output mapping="sum" symbol-ref="neigh"/>
            </NeighborhoodReporter>
            <Disabled>
                <Property value="0.0" symbol="ky"/>
            </Disabled>
            <Property value="0.0" symbol="avspeed"/>
            <Property value="0.0" name="Average Speed * 100" symbol="truavspeed"/>
        </CellType>
    </Disabled>
-->
        <CellType class="biological" name="dividingcell">
            <!--    <Disabled>
        <Chemotaxis field="c" contact-inhibition="true" strength="1000*(C1+C2*(2*R/(C3+R)))"/>
    </Disabled>
-->
            <VolumeConstraint strength="1" target="50"/>
            <!--    <Disabled>
        <VolumeConstraint target="25*(1+ky/5)" strength="1"/>
    </Disabled>
-->
            <!--    <Disabled>
        <VolumeConstraint target="25*(1+ky/10+(cell.center.x>0.1*size.x)*(time>tlim)*(M/10))" strength="1"/>
    </Disabled>
-->
            <ConnectivityConstraint/>
            <SurfaceConstraint strength="1" target="1" mode="aspherity"/>
            <!--    <Disabled>
        <CellDivision division-plane="major">
            <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;size.x*0.1)</Condition>
            <Triggers>
                <Rule symbol-ref="Y">
                    <Expression>max(0.7 + 0.3*rand_norm(2,3),0)</Expression>
                </Rule>
                <Rule symbol-ref="E">
                    <Expression>max(0.1+rand_norm(0.9,0.5),0)</Expression>
                </Rule>
                <Rule symbol-ref="R">
                    <Expression>max(0.35+rand_norm(0.1,0.1),0)</Expression>
                </Rule>
            </Triggers>
        </CellDivision>
    </Disabled>
-->
            <Property value="0" name="YAP" symbol="Y"/>
            <Property value="10" name="E-cadherin" symbol="E"/>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R">
                    <Expression>(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <Rule symbol-ref="Cr">
                    <Expression>Cr+0.1*(cell.center.y>0.52*size.y)*(time&lt;2)*(Cr&lt;0.1)</Expression>
                </Rule>
                <Rule symbol-ref="avspeed">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule symbol-ref="dist">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
            </System>
            <Property value="0" symbol="R"/>
            <!--    <Disabled>
        <PersistentMotion strength="0.15" decay-time="20"/>
    </Disabled>
-->
            <DirectedMotion strength="0.2*(C1+C2*(2*R/(C3+R)))" direction="1, 0.0, 0.0"/>
            <PropertyVector value="0.0, 0.0, 0.0" name="Speed" symbol="d"/>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <!--    <Disabled>
        <Property value="0" symbol="kyr"/>
    </Disabled>
-->
            <NeighborhoodReporter>
                <Input value="ky" scaling="cell"/>
                <Output symbol-ref="av" mapping="average"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output symbol-ref="M" mapping="sum"/>
            </NeighborhoodReporter>
            <Property value="0.0" symbol="M"/>
            <Property value="0.0" symbol="av"/>
            <Property value="0.0" symbol="neigh"/>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.dividingcell.id" scaling="cell"/>
                <Output symbol-ref="neigh" mapping="sum"/>
            </NeighborhoodReporter>
            <!--    <Disabled>
        <Property value="0" symbol="ky"/>
    </Disabled>
-->
            <Property value="0.0" symbol="avspeed"/>
            <Property value="0.0" name="Average Speed * 100" symbol="truavspeed"/>
            <CellDeath>
                <Condition>(cell.center.x>0.95*size.x)</Condition>
            </CellDeath>
            <Property value="0.0" name="distance from sheet edge" symbol="dist"/>
            <Property value="0.0" symbol="Cr"/>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population size="1" type="dividingcell">
            <InitRectangle number-of-cells="1" mode="regular">
                <Dimensions size="size.x/100, size.y/100, size.z" origin="size.x/100, size.y/4, 0"/>
            </InitRectangle>
        </Population>
        <!--    <Disabled>
        <Population size="1" type="addedcell">
            <InitRectangle number-of-cells="10" mode="regular">
                <Dimensions size="size.x/100, 0.9*size.y, size.z" origin="size.x/100, 0.05*size.y, 0"/>
            </InitRectangle>
        </Population>
    </Disabled>
-->
        <Population size="1" type="dividingcell">
            <InitRectangle number-of-cells="1" mode="regular">
                <Dimensions size="size.x/100, size.y/100, size.z" origin="size.x/100, size.y/4+2, 0"/>
            </InitRectangle>
        </Population>
        <Population size="1" type="dividingcell">
            <InitRectangle number-of-cells="1" mode="regular">
                <Dimensions size="size.x/100, size.y/100, size.z" origin="size.x/100, size.y/2+3, 0"/>
            </InitRectangle>
        </Population>
        <Population size="1" type="dividingcell">
            <InitRectangle number-of-cells="1" mode="regular">
                <Dimensions size="size.x/100, size.y/100, size.z" origin="size.x/100, size.y/2+1, 0"/>
            </InitRectangle>
        </Population>
        <Population size="1" type="dividingcell">
            <InitRectangle number-of-cells="1" mode="regular">
                <Dimensions size="size.x/100, size.y/100, size.z" origin="size.x/100, size.y-5, 0"/>
            </InitRectangle>
        </Population>
        <Population size="1" type="dividingcell">
            <InitRectangle number-of-cells="1" mode="regular">
                <Dimensions size="size.x/100, size.y/100, size.z" origin="size.x/100, size.y-7, 0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact value="15" type1="dividingcell" type2="dividingcell">
                <!--    <Disabled>
        <HomophilicAdhesion adhesive="(A1+A2*(E>A3))" strength="-10"/>
    </Disabled>
-->
                <!--    <Disabled>
        <HeterophilicAdhesion strength="10.0" adhesive1="A1+A2*(E>A3)" adhesive2="A1+A2*(E>A3)"/>
    </Disabled>
-->
                <AddonAdhesion strength="5" adhesive="A1+A2*(2*E/(A3+E))"/>
            </Contact>
            <Contact value="12" type1="dividingcell" type2="medium"/>
            <!--    <Disabled>
        <Contact value="15" type1="addedcell" type2="addedcell">
            <Disabled>
                <HomophilicAdhesion adhesive="A1+A2*(E>A3)" strength="10.0"/>
            </Disabled>
            <AddonAdhesion adhesive="A1+A2*(E>A3)" strength="10.0"/>
        </Contact>
    </Disabled>
-->
            <!--    <Disabled>
        <Contact value="12" type1="addedcell" type2="medium"/>
    </Disabled>
-->
            <!--    <Disabled>
        <Contact value="12" type1="addedcell" type2="wall"/>
    </Disabled>
-->
            <!--    <Disabled>
        <Contact value="15" type1="leadercell" type2="dividingcell">
            <AddonAdhesion adhesive="A1" strength="10.0"/>
        </Contact>
    </Disabled>
-->
            <!--    <Disabled>
        <Contact value="15" type1="leadercell" type2="leadercell">
            <AddonAdhesion adhesive="A1" strength="10.0"/>
        </Contact>
    </Disabled>
-->
            <!--    <Disabled>
        <Contact value="12" type1="leadercell" type2="medium"/>
    </Disabled>
-->
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
