<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

Produced SI Figure 14

Implementing the TGFbeta signaling system in a wound-healing cell sheet. The signaling system is based on the ODEs in 

Tian, X.J., Zhang, H. and Xing, J., 2013. Coupled reversible and irreversible bistable switches underlying TGFβ-induced epithelial to mesenchymal transition. Biophysical journal, 105(4), pp.1079-1089.

Equations (ODEs) and parameter values were kindly supplied by Prof. Xiao-Jun Tian

A static gradient of TGF beta is assumed. There are three states visible, including an intermediate EM state.  

This file was constructed by Leah Edelstein-Keshet 

         </Details>
        <Title>TianTGFbetaCellSheet</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="400, 100, 0"/>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="noflux" boundary="-x"/>
                <Condition type="periodic" boundary="y"/>
                <Condition type="periodic" boundary="-y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="stoptime" value="3000"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Global>
        <Field symbol="TGFex" name="TGFex" value="5*(space.x/size.x)"/>
        <!--
      <Constant value="0.1" name="TGF0" symbol="TGF0"/>
  -->
        <Constant symbol="k0TGF" name="k0TGF" value="0.06"/>
        <Constant symbol="kTGF" name="kTGF" value="1.2"/>
        <Constant symbol="JTGF" name="JTGF" value="0.06"/>
        <Constant symbol="kdTGF" name="kdTGF" value="0.6"/>
        <Constant symbol="Jsnail" name="Jsnail" value="1.6"/>
        <Constant symbol="kdsnail" name="kdsnail" value="0.09"/>
        <Constant symbol="k0snail" name="k0snail" value="0.0006"/>
        <Constant symbol="ksnail" name="ksnail" value="0.03"/>
        <Constant symbol="kSNAIL" name="kSNAIL" value="17.0"/>
        <Constant symbol="JSNAIL" name="JSNAIL" value="0.08"/>
        <Constant symbol="kdSNAIL" name="kdSNAIL" value="1.66"/>
        <Constant symbol="Jsnailcad1" value="0.2"/>
        <Constant symbol="k034" name="k034" value="0.0012"/>
        <Constant symbol="k34" name="k34" value="0.012"/>
        <Constant symbol="J134" name="J134" value="0.15"/>
        <Constant symbol="J234" name="J234" value="0.36"/>
        <Constant symbol="kd34" name="kd34" value="0.035"/>
        <Constant symbol="k0zeb" name="k0zeb" value="0.003"/>
        <Constant symbol="kzeb" name="kzeb" value="0.06"/>
        <Constant symbol="Jzeb" name="Jzeb" value="3.5"/>
        <Constant symbol="kdzeb" name="kdzeb" value="0.09"/>
        <Constant symbol="kZEB" name="kZEB" value="17"/>
        <Constant symbol="JZEB" name="JZEB" value="0.06"/>
        <Constant symbol="kdZEB" name="kdZEB" value="1.66"/>
        <Constant symbol="k02" name="k02" value="0.0002"/>
        <Constant symbol="k2" name="k2" value="0.012"/>
        <Constant symbol="kd2" name="kd2" value="0.035"/>
        <Constant symbol="J12" name="J12" value="5.0"/>
        <Constant symbol="J22" name="J22" value="0.2"/>
        <Constant symbol="kecad1" value="0.1"/>
        <Constant symbol="kecad2" value="0.06"/>
        <Constant symbol="Jecad1" value="0.2"/>
        <Constant symbol="Jecad2" value="0.5"/>
        <Constant symbol="kdecad" value="0.05"/>
        <Constant symbol="kncad1" value="0.1"/>
        <Constant symbol="kncad2" value="0.06"/>
        <Constant symbol="Jncad1" value="0.2"/>
        <Constant symbol="Jncad2" value="0.5"/>
        <Constant symbol="kdncad" value="0.05"/>
        <Variable symbol="Ecadherin" name="Ecadherin" value="0.0"/>
    </Global>
    <CellTypes>
        <CellType name="medium" class="medium"/>
        <CellType name="dividingcell" class="biological">
            <VolumeConstraint target="50" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint target="1" mode="aspherity" strength="1"/>
            <LengthConstraint target="4+4*Ncadherin" mode="length" strength="2"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(0.4))* (cell.center.x&lt;50)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion name="cell migration" direction="1, 0.0, 0.0" strength="1+Ncadherin/3"/>
            <Property symbol="TGF0" name="TGF0" value="0"/>
            <Property symbol="TGF" name="TGF" value="0.1"/>
            <Property symbol="snail" name="snail" value="0.01"/>
            <Property symbol="SNAIL" name="SNAIL" value="0.01"/>
            <Property symbol="miR34" name="miR34" value="0.4"/>
            <Property symbol="zeb" name="zeb" value="0.01"/>
            <Property symbol="ZEB" name="ZEB" value="0.01"/>
            <Property symbol="miR200" name="miR200" value="0.4"/>
            <Property symbol="Ecadherin" name="E-cadherin" value="4"/>
            <Property symbol="Ncadherin" name="N-cadherin" value="0"/>
            <PropertyVector symbol="d" name="speed" value="0.0, 0.0, 0.0"/>
            <Mapper name="sensing external TGFbeta">
                <Input value="TGFex"/>
                <Output mapping="average" symbol-ref="TGF0"/>
            </Mapper>
            <!--
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.medium.id" scaling="length"/>
                <Output mapping="sum" symbol-ref="M"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input value="cell.type == celltype.dividingcell.id" scaling="cell"/>
                <Output mapping="sum" symbol-ref="neigh"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>


    -->
            <System solver="Runge-Kutta [fixed, O(4)]" time-step="0.1">
                <DiffEqn name="Equation for TGF" symbol-ref="TGF">
                    <Expression>k0TGF+kTGF/(1+(miR200/JTGF)^2)-kdTGF*TGF</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for snail" symbol-ref="snail">
                    <Expression>k0snail+ksnail*(((TGF+TGF0)/Jsnail)^2)/(1+((TGF+TGF0)/Jsnail)^2)-kdsnail*snail</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for SNAIL" symbol-ref="SNAIL">
                    <Expression>kSNAIL*snail/(1+(miR34/JSNAIL)^2)-kdSNAIL*SNAIL</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for miR34" symbol-ref="miR34">
                    <Expression>k034+k34/(1+(SNAIL/J134)^2+(ZEB/J234)^2)-kd34*miR34</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for zeb" symbol-ref="zeb">
                    <Expression>k0zeb+kzeb*(SNAIL/Jzeb)^2/(1+(SNAIL/Jzeb)^2)-kdzeb*zeb</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for ZEB" symbol-ref="ZEB">
                    <Expression>kZEB*zeb/(1+(miR200/JZEB)^2)-kdZEB*ZEB</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for miR200" symbol-ref="miR200">
                    <Expression>k02+k2/(1+(SNAIL/J12)^2+(ZEB/J22)^2)-kd2*miR200</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for E-cadherin" symbol-ref="Ecadherin">
                    <Expression>kecad1/((SNAIL/Jecad1)^2+1)+kecad2/((ZEB/Jecad2)^2+1)-kdecad*Ecadherin</Expression>
                </DiffEqn>
                <DiffEqn name="Equation for N_cadherin" symbol-ref="Ncadherin">
                    <Expression>kncad1*((SNAIL/Jncad1)^2)/((SNAIL/Jncad1)^2+1)+kncad2*((ZEB/Jncad2)^2)/((ZEB/Jncad2)^2+1)-kdncad*Ncadherin</Expression>
                </DiffEqn>
                <!--
                <Rule name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr" symbol-ref="Cr">
                    <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
                </Rule>
                <Rule name="distance from right domain edge" symbol-ref="dist">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule name="Sum of instantaneous speeds each time step (times 100)" symbol-ref="avspeed">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule name="average speed over the simulation (times 100)" symbol-ref="truavspeed">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule name="Sum of instantaneous speeds over final 100 time steps (times 100)" symbol-ref="avspeed2">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule name="average speed over 100 time steps (times 100)" symbol-ref="truavspeed2">
                    <Expression>avspeed2/200</Expression>
                </Rule>
  -->
            </System>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population name="Initialize cell sheet at left edge of the domain" type="dividingcell" size="1">
            <!--    <Disabled>
        <InitRectangle number-of-cells="70" mode="regular">
            <Dimensions origin="size.x/100, 0, 0" size="size.x/100, size.y, size.z"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle number-of-cells="70" mode="regular">
                <Dimensions origin="0.0, 0.0, 0.0" size="4.0, size.y, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact type2="dividingcell" type1="dividingcell" value="30">
                <AddonAdhesion name="Adhesion" strength="5" adhesive="12*Ecadherin/(1+Ecadherin)"/>
            </Contact>
            <Contact type2="medium" type1="dividingcell" value="12"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
    <Analysis>
        <Gnuplotter decorate="true" time-step="50">
            <Terminal name="png"/>
            <Plot>
                <Cells min="0" flooding="true" value="Ecadherin">
                    <!--
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.5" color="light-blue"/>
                            <Color value="1" color="light-red"/>
                        </ColorMap>
  -->
                </Cells>
                <Field symbol-ref="TGFex"/>
            </Plot>
            <Plot>
                <Cells min="0" flooding="true" value="Ncadherin">
                    <!--
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.5" color="light-blue"/>
                            <Color value="1" color="light-red"/>
                        </ColorMap>
  -->
                </Cells>
                <Field symbol-ref="TGFex"/>
            </Plot>
        </Gnuplotter>
        <!--
        <Logger time-step="1.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="300">
                    <Style point-size="0.05" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="0.0" maximum="size.x">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis minimum="0.0" maximum="size.y">
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar minimum="0.0" palette="rainbow" maximum="0.3">
                        <Symbol symbol-ref="d.abs"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
  -->
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
