---
MorpheusModelID: M2063

authors: [E. G. Rens, L. Edelstein-Keshet]
contributors: [E. G. Rens]

title: "Rac-Rho-ECM Spatial Model with Catch-Slip Bond Integrin Biophysics"

# Reference details
publication:
  doi: "10.1088/1478-3975/ac2888"
  title: "Cellular Tango: how extracellular matrix adhesion choreographs Rac-Rho signaling and cell movement"
  journal: "Phys. Biol."
  volume: 18
  issue: 6
  page: "066005"
  year: 2021
  original_model: true

tags:
- 1D
- Adhesion
- Catch-Slip Bond
- Cell Deformation
- Cell-ECM Adhesion
- Cell Shape
- Cellular Potts Model
- CPM
- ECM
- Extracellular Matrix
- Front Protrusion
- GTPase
- GTPase Signaling
- Oscillation
- Partial Differential Equation
- PDE
- Persistent Polarity
- Polarity Oscillation
- Protrusion-Retraction
- Rac
- Reaction–Diffusion System
- Rear Retraction
- Rho
- Spatial Pattern
- Spiral Wave

categories:
- DOI:10.1088/1478-3975/ac2888

# order
#weight: 30
---
## Introduction

This paper describes the regimes of behaviour of a 1D spatial (PDE) model for the mutually antagonistic Rac-Rho GTPases, with feedback to and from the extracellular matrix (ECM).

## Description

The [Rac-Rho submodels](/category/doi10.1088/1478-3975/ac2888/) are bistable, and ECM enhances Rho activation. Rac and Rho contribute positive and negative feedback, respectively, to the ECM. The full model has regimes of uniform, polar, random, and oscillatory dynamics.

## Results

The [file listed below](#model) was used to produce [Figure 6](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888#pbac2888f6), Model III: the catch-slip bond model in 1D, showing regimes of no pattern, polarized patterns, and some oscillatory patterns. 

![](RensKeshetModel3image.jpg "[Fig. 6](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888#pbac2888f6) showing Model III regimes ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Rens *et al.***](#reference))")