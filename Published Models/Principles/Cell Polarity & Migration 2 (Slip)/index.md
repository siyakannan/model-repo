---
MorpheusModelID: M2062

authors: [E. G. Rens, L. Edelstein-Keshet]
contributors: [E. G. Rens]

title: "Rac-Rho-ECM Spatial Model with Slip-Bond Integrin Dynamics"

# Reference details
publication:
  doi: "10.1088/1478-3975/ac2888"
  title: "Cellular Tango: how extracellular matrix adhesion choreographs Rac-Rho signaling and cell movement"
  journal: "Phys. Biol."
  volume: 18
  issue: 6
  page: "066005"
  year: 2021
  original_model: true

tags:
- 1D
- Adhesion
- Cell Deformation
- Cell-ECM Adhesion
- Cell Shape
- Cellular Potts Model
- CPM
- ECM
- Extracellular Matrix
- Front Protrusion
- GTPase
- GTPase Signaling
- Integrin
- Integrin Dynamics
- Oscillation
- Partial Differential Equation
- PDE
- Persistent Polarity
- Polarity Oscillation
- Protrusion-Retraction
- Rac
- Reaction–Diffusion System
- Rear Retraction
- Rho
- Slip-Bond
- Spatial Pattern
- Spiral Wave

categories:
- DOI:10.1088/1478-3975/ac2888

# order
#weight: 20
---
## Introduction

This paper describes the regimes of behaviour of a 1D spatial (PDE) model for the mutually antagonistic Rac-Rho GTPases, with feedback to and from the extracellular matrix (ECM).

## Description

The [Rac-Rho submodels](/category/doi10.1088/1478-3975/ac2888/) are bistable, and ECM enhances Rho activation. Rac and Rho contribute positive and negative feedback, respectively, to the ECM. The full model has regimes of uniform, polar, random, and oscillatory dynamics.

## Results

The [file listed below](#model) was used to produce [supplementary Figure 2](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888/pdf). Model II regimes: As in [SI Fig. 1](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888/pdf), but for the slip-bond integrin model in the ECM dynamics PDE. 

![](RensKeshetModel2image.jpg "[Suppl. Fig. 2](https://iopscience.iop.org/article/10.1088/1478-3975/ac2888/pdf) showing Model II regimes ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/): [**Rens *et al.***](#reference))")