---
MorpheusModelID: M6694

title: "Diffusion Model of Gap Genes in Drosophila melanogaster"

authors: [M. R. Regueira, J. D. García, A. R.-P. Aradas]
contributors: [M. R. Regueira]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1101/579342"
  title: "The multicellular incoherent feedforward loop motif generates spatial patterns"
  journal: "bioRxiv"
  #volume: 
  #issue: 
  #page: ""
  year: 2019
  original_model: true
  preprint: true

#categories:
#- DOI:10.1101/579342

tags:
- Anterior-Posterior Axis
- Anterior-Posterior Patterning
- A-P Axis
- A-P Patterning
- Bicoid
- bcd
- Blastoderm
- Blastoderm Stage
- cad
- Caudal
- Cellular Potts Model
- CPM
- Domain:Image
- Drosophila
- Drosophila melanogaster
- Embryogenesis
- Embryo Segmentation
- eve
- even-skipped
- Feed-Forward Loop
- FFL
- IFFL
- Irregular Domain
- Gap Gene
- Giant
- gt
- hb
- Hunchback
- Incoherent Feed-Forward Loop
- kni
- Knirps
- kr
- Krüppel
- Maternal Effect Gene
- Morphogen
- Morphogenesis
- Multicellular Incoherent Feed-Forward Loop
- Multiscale Model
- mIFFL
- Nanos
- nos
- ODE
- Ordinary Differential Equation
- Pair-Rule Gene
- Partial Differential Equation
- Pattern Formation
- PDE
- Segmentation
- Spatial Pattern
- Stripe Pattern
- Tailless
- Terminal Gene
- tll
---
> Autoregulatory multicellular incoherent feedforward loop (mIFFL) that generates the 7-stripe even-skipped (eve) expression pattern in *Drosophila melanogaster* embryogenesis

## Introduction

This model demonstrates the influence of maternal effect genes and gap genes on the even-skipped (eve) gene. Its expression produces a series of seven bands during *Drosophila melanogaster* embryogenesis and plays a key role in the regulation of the Drosophila segmentation pattern. Here, the even-skipped gene stripe pattern is obtained using an autoregulatory multicellular incoherent feedforward loop (mIFFL), which is an extension of the traditional intracellular IFFL gene motif in which the interacting nodes no longer need to be genes inside the same cell, but can be spatially distributed in different cells.

## Description

Simulations were performed using a mask of a real *Drosophila melanogaster* embryo at the beginning of anterior-posterior segmentation, when the embryo is in the blastoderm stage.

![](M6694_figure-s5.1_embryo-mask.jpg "Lateral view of the gastrulating embryo of *D. melanogaster* (left) and the derived mask [`M6694_model_domain.tiff`](#model) (right). (© [Regueira *et al.*, 2019](#reference), Fig. S5.1)")

The actual number of cells at this stage is about 6000, while the model involves only 2800 cells, since it only captures the external surface of one lateral face of the embryo. The size of the embryo at this stage is roughly $`0.5\ \mathrm{mm}`$ in length. ([Regueira *et al.*, 2019](#reference))

The simulation is based on the interaction network depicted below, in which the expression of the maternal effect genes act as inputs, and the gap-gene spatial pattern is generated dynamically from them.

![](M6694_figure-4a_gap-gene-network.jpg "Interaction network of the gap genes and maternal effect genes. (© [Regueira *et al.*, 2019](#reference), Fig. 4A)")

Once the gap genes are fully expressed, the expression of pair-rule genes and, therefore, eve begins. Ultimately, the eve bands are produced by an autoregulatory multicellular incoherent feedforward loop (mIFFL).

## Results

The eve gene is expressed in seven bands that are arranged perpendicular to the anterior-posterior axis of the embryo. Not only the number of stripes, but also their width and the distance between are well reproduced in comparison with the experimental observations.

![](M6694_figure-5c_pattern-in-vivo-vs-in-silico.jpg "Formation of the Eve stripes during time. **Left and top:** Once Eve stripes are formed, they emit two signals that incoherently regulate the emergence of the next stripe. The first signal is fast and short-range (red) and blocks the activation of eve near an existing band. The second signal (blue) is slower and long-range and suppresses the constitutive repressor, leading to activation of eve expression. **Bottom right:** The final *in silico* pattern closely resembles the *in vivo* eve phenotype. (© [Regueira *et al.*, 2019](#reference), Fig. 5C)")

![](M6694_video_simulation.mp4)
**Video of the model simulation {{< model_quick_access "media/model/m6694/M6694_model_mIFFL.xml" >}}:** Sequential formation of bands. Stripes 2 and 7 appear first. The adjacent bands appear after the incoherent regulation over space following the logic outlined above in Fig. 4A by [Regueira *et al.*, 2019](#reference).