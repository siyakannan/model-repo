---
MorpheusModelID: M0237

authors: [M. Kücken, L. Brusch]
contributors: [M. Kücken]

title: Planarian Regeneration (Double Head)
date: "2019-11-18T00:00:00+01:00"
lastmod: "2021-07-26T18:54:00+02:00"

tags:
- β-catenin
- Body Plan Polarity
- Cell Polarity
- Ciliary Rootlet
- Epidermis
- Flatworm
- Fat/Dachsous Signaling Pathway
- Ft/Ds Pathway
- MCC
- Multiciliated Cell
- PCP Pathway
- Planar Cell Polarity
- Planarian
- Planarian Regeneration
- Planar Polarity Regulation
- Polarity
- Polarity Field
- Polarization
- Spatio-Temporal Model

- Double Head
- time-step

categories:
- DOI:10.1016/j.devcel.2019.10.022
---

>Spatio-temporal model of planar polarity regulation during planarian regeneration

## Introduction

This simulation reproduces Figure 4D of the [referenced paper](#reference). Parameter values correspond to the *β-catenin-1(RNAi)* pattern as given in Table S2 of the referenced paper.

## Description

The model explores the repolarization of the animal after the induction of an ectopic head. $(A_x, A_y)$ represents the A/P polarization system, $(M_x, M_y)$ represents the M/L polarization system and $(P_x, P_y)$ the resulting superimposed and normalized polarization. $(P_x, P_y)$ is compared to the measured polarity data of ciliary rootlets. The induction of the ectopic head occurs at ```time-step``` $200$ at the position where the former tail was (right end).

![Movie of the simulation](sim-double-head.mp4)

## Reference

This model is described in the peer-reviewed publication:

>H. T.-K. Vu, S. Mansour, M. Kücken, C. Blasse, C. Basquin, J. Azimzadeh, E. W. Myers, L. Brusch, J. C. Rink: [Dynamic Polarization of the Multiciliated Planarian Epidermis between Body Plan Landmarks][reference]. *Dev. Cell* **51** (4): 526-542.e6, 2019.

[reference]: https://doi.org/10.1016/j.devcel.2019.10.022