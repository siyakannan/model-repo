---
MorpheusModelID: M2984

authors: [V. Lupperger, C. Marr, P. Chapouton]
contributors: [V. Lupperger, L. Brusch]

title: "Neural Stem Cell Redivisions in Adult Zebrafish Telencephalon"
date: "2021-06-15T13:00:00+01:00"
lastmod: "2021-07-26T20:00:00+02:00"

# Reference details
publication:
  doi: "10.1371/journal.pbio.3000708"
  title: "Reoccurring neural stem cell divisions in the adult zebrafish telencephalon are sufficient for the emergence of aggregated spatiotemporal patterns"
  journal: "PLoS Biology"
  volume: 18
  issue: 12
  page: "e3000708"
  year: 2020
  original_model: true

tags:
- Brain
- Cellular Potts Model
- Cell Cycle
- CPM
- Neural Stem Cell
- NSC
- Parameter Optimization
- Progenitor Cell
- Spatio-Temporal Patterning
- S-phase
- Stem Cell
- Telencephalon
- Zebrafish

categories:
- DOI:10.1371/journal.pbio.3000708
---

## Introduction

Whether individual stem cell divisions are coordinated or random is important for tissue morphogenesis, homeostasis and tumour growth. [Lupperger *et al.*](#reference) have analyzed the spatio-temporal division patterns of adult neural stem cells (NSC) in the zebrafish telencephalon. Imaging of over 80&#8239;000 NSCs in 36 brain hemispheres and comparison to model simulations with Morpheus revealed that redivisions (rapid cell cycle reentry by some newly born NSCs) can explain the observed aggregated division pattern.

## Description

- **Units** in the model are $\[\text{space}\] = \mathrm{\mu m}$, $\[\text{time}\] = 0.01\ \text{hours}$.
- **Parameter values** were fitted by [Lupperger *et al.*](#reference) to *in vivo* labelling experiments. `p_div` = $9 \cdot 10^{−4}$ divisions per hour (amounts to $9 \cdot 10^{-6}$ divisions per Monte Carlo Step in the model) and `p_rediv` = $0.38$ (which can be adjusted in the model section `Global`). The differentiation process is set to $10\ \%$.
- **Initial condition**: $500$ cells, simulated until $2.356 \pm 460$ (mean ± SD) cells are reached, as in experiments.
- The [**published model file**][original-model] was originally developed with Morpheus version 2.0.1 and is shared by the authors.
- The **new model** file provided [below](#model) has been updated (solver naming, added reference and links to description, moved parameters `p_rediv`, `init_volume` from `CellType` definition to `Global` to ease future modifications of their values) such that it works with [Morpheus version 2.2.3 and later](/download/latest/) and **reproduces the original results**.

[original-model]: original_model_redivision.xml

## Results

The model simulates the growth of the zebrafish telencephalon in a 2D cross section and visualizes NSCs in green, S-phase NSCs in red and progenitor cells in black as was presented in the published Figure S4G,H (shown below).

![](original_Fig.S4GH.png "Model results as published by [Lupperger *et al.*](#reference) ([Figure S4G,H](https://doi.org/10.1371/journal.pbio.3000708.s004)). [*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/)")

Running the [model](#model) for 150&#8239;000 Monte Carlo steps (1&#8239;500 hours of brain development) yields 2&#8239;726 cells and reproduces the aggregated division pattern.

![](reproduced_Fig.S4GH.png "Model results reproduced with this Morpheus model. Figure was assembled from `plot-1*.png` and `plot-2*.pdf` output files.")

The spatiotemporal dynamics from 20&#8239;000-150&#8239;000 Monte Carlo steps is shown in the following Video. The left panel shows each cell clone in a unique color and the right panel shows the cell states color-coded as in the above Figures.

![Video of the model simulation from 20&#8239;000-150&#8239;000 Monte Carlo steps.](reproduced_Fig.S4G.mp4)
