<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>Cytotoxic T Lymphocytes
</Title>
        <Details>Full title:	Spatial Effects on Killing Kinetics of Cytotoxic T Lymphocytes
Date:	08.09.2021
Authors:	R. J. Beck, D. I. Bijker, J. B. Beltman
Curators:	J. B. Beltman, D. Jahn
Software:	Morpheus (open source). Download from: https://morpheus.gitlab.io
Model ID:	https://identifiers.org/morpheus/M9495
Units:	[time] = s
Reference:	This model is the original used in the publication, up to technical updates:
	R. J. Beck, D. I. Bijker, J. B. Beltman: Heterogeneous, delayed-onset killing by multiple-hitting T cells: Stochastic simulations to assess methods for analysis of imaging data. PLoS Comput. Biol. 16 (7): 1-25, 2020.
	https://doi.org/10.1371/journal.pcbi.1007972</Details>
    </Description>
    <Global>
        <Constant symbol="tumble.run_duration" value="0"/>
        <Variable symbol="ctl.contact.length" value="0.0"/>
        <!--    <Disabled>
        <VariableVector symbol="move_dir" value="0.0, 0.0, 0.0"/>
    </Disabled>
-->
        <Field symbol="act" value="0.0"/>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="size" value="100, 100, 0"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="constant" boundary="y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="43200"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="CTL">
            <VolumeConstraint target="140" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint target="1" mode="aspherity" strength="0.1"/>
            <Property symbol="ctl.attn" value="2"/>
            <NeighborhoodReporter name="ctl contact reporter">
                <Input value="cell.type==celltype.target.id" scaling="length"/>
                <Output symbol-ref="ctl.contact.length" mapping="sum"/>
            </NeighborhoodReporter>
            <Protrusion field="act" maximum="50" strength="10"/>
        </CellType>
        <CellType class="medium" name="medium"/>
        <CellType class="biological" name="target">
            <VolumeConstraint target="vol.target" strength="1"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint target="1" mode="aspherity" strength="0.25"/>
            <CellDeath>
                <Condition>is.dead</Condition>
            </CellDeath>
            <NeighborhoodReporter name="contact.reporter">
                <Output symbol-ref="ctl.attn" mapping="sum"/>
                <Input value="(cell.type==celltype.CTL.id)/max(1,ctl.contact.length)" scaling="length"/>
            </NeighborhoodReporter>
            <Property symbol="at.risk" value="0.0"/>
            <Property symbol="hits.received" value="0.0"/>
            <Property symbol="is.dead" value="0.0"/>
            <Constant symbol="hit.chance" value="nhits/3600"/>
            <Event trigger="when-true" name="hitting" time-step="1">
                <Condition>rand_uni(0,1)&lt;hit.chance*(maturity>=minhit)*(1-is.dead)*ctl.attn</Condition>
                <Rule symbol-ref="hits.received">
                    <Expression>hits.received + 1
</Expression>
                </Rule>
                <Rule symbol-ref="is.dead">
                    <Expression>(hits.received+1)>=nhits
</Expression>
                </Rule>
            </Event>
            <Constant symbol="h50" value="2"/>
            <Constant symbol="c" value="20"/>
            <Property symbol="ctl.attn" value="0.0"/>
            <Property symbol="vol.target" value="340"/>
            <Constant symbol="nhits" value="5"/>
            <Event trigger="when-true" name="maturing" time-step="60">
                <Condition>ctl.attn>0</Condition>
                <Rule symbol-ref="maturity">
                    <Expression>maturity+1</Expression>
                </Rule>
            </Event>
            <Property symbol="maturity" value="0.0"/>
            <Event trigger="when-true" name="immaturing" time-step="60">
                <Condition>ctl.attn==0</Condition>
                <Rule symbol-ref="maturity">
                    <Expression>0</Expression>
                </Rule>
            </Event>
            <Constant symbol="minhit" value="0"/>
        </CellType>
        <CellType class="biological" name="well">
            <FreezeMotion>
                <Condition>1</Condition>
            </FreezeMotion>
            <Property symbol="ctl.attn" value="-1"/>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction default="0">
            <Contact value="0" type1="CTL" type2="medium"/>
            <Contact value="0" type1="target" type2="medium"/>
            <Contact value="-3" type1="CTL" type2="target"/>
            <Contact value="0.7" type1="target" type2="target"/>
            <Contact value="0" type1="well" type2="CTL"/>
            <Contact value="0" type1="well" type2="target"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <Neighborhood>
                <Order>3</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="1.2"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Distance>2.5</Distance>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="CTL" size="1">
            <InitCircle number-of-cells="1" mode="random">
                <Dimensions radius="40.0" center="50.0, 50.0, 0.0"/>
            </InitCircle>
        </Population>
        <Population type="target" size="0">
            <InitCircle number-of-cells="rand_norm(16,4)" mode="random">
                <Dimensions radius="40.0" center="50.0, 50.0, 0.0"/>
            </InitCircle>
            <!--    <Disabled>
        <InitProperty symbol-ref="vol.target">
            <Expression>rand_norm(240,40)</Expression>
        </InitProperty>
    </Disabled>
-->
        </Population>
        <Population type="well" size="1">
            <TIFFReader keep_ID="false" filename="boundary.tiff"/>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter decorate="false" time-step="60">
            <Terminal name="png"/>
            <Plot>
                <Cells value="ctl.attn">
                    <ColorMap>
                        <Color value="0" color="gray80"/>
                        <Color value="0.01" color="gray40"/>
                        <Color value="1" color="blue"/>
                        <Color value="-1" color="black"/>
                        <Color value="2" color="red"/>
                    </ColorMap>
                </Cells>
                <Disabled>
                    <CellLabels value="hits.received"/>
                </Disabled>
                <CellLabels fontsize="20" value="hits.received"/>
                <Disabled>
                    <CellArrows style="1" orientation="move_dir"/>
                </Disabled>
            </Plot>
            <Plot>
                <Field symbol-ref="act"/>
                <Cells value="maturity" min="0.5" max="1.5"/>
            </Plot>
        </Gnuplotter>
        <Logger time-step="60">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="cell.type"/>
                <Symbol symbol-ref="ctl.attn"/>
                <Symbol symbol-ref="celltype.target.size"/>
                <Symbol symbol-ref="hits.received"/>
                <Symbol symbol-ref="cell.id"/>
                <Symbol symbol-ref="maturity"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
        </Logger>
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
