---
MorpheusModelID: M0015
title: "3D Reaction-Diffusion: Excitable Media"
date: "2019-11-06T14:35:00+01:00"
lastmod: "2020-10-30T12:34:00+01:00"

aliases: [/examples/3d-reaction-diffusion-excitable-media/]

menu:
  Built-in Examples:
    parent: PDE
    weight: 50
weight: 100
---

## Introduction

This example uses the [Barkley model of excitable media](http://www.scholarpedia.org/article/Barkley_model), similar to the [Fitzhugh-Nagumo model](https://en.wikipedia.org/wiki/FitzHugh%E2%80%93Nagumo_model) to show how to model and visualize reaction-diffusion models in 3D.

![](barkley_3d.png "Scroll wave appears in Barkley model of excitable media in 3D.")

## Description

This model defines a 3D ```cubic``` ```Lattice``` with ```noflux``` ```BoundaryConditions```. Two ```Layers``` are defined for the two species: $u$ is the signal, and $v$ the refractoriness. As in the examples above, the ```DiffEqn``` as specified in the ```System``` in ```PDE```. Nothing strange here.

To visualize the resulting scrolling waves in 3D, the ```TiffPlotter``` is used. This ```Analysis``` plugin writes TIFF image stacks that can be opened by image analysis software such as [Fiji ImageJ](http://fiji.sc/). To import Morpheus TIFF images into Fiji, macro scripts are available that help you to create 3D ($xyz$), 4D ($xyzt$) or even 5D ($xyzct$) images and movies of your simulations.

Although unable to plot 3D, the ```GnuPlotter``` can still be helpful to plot a 2D slice. See ```Analysis/Gnuplotter/PDE/slice```. 

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47172325?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Import resulting sequence of TIFF images in ImageJ or Fiji, and create 4D movie using ImageJ's 3D plugin:
    - Open ```u_v.tif``` in ImageJ: ```File``` → ```Open```.
    - Create hyperstack: ```Image``` → ```Hyperstack``` → ```Convert to Hyperstack```. ```Channels``` ($c$): ```2```, ```Slices``` ($z$): ```50```, ```Frames``` ($t$): ```51```, ```Display Mode```: ```Composite```.
    - Display in 4D: ```Plugins``` → ```3D Viewer```. ```Use default parameters```. Press ```OK```.

## Reference

D. Barkley: [A model for fast computer simulation of waves in excitable media.][barkley-1991] Physica 49D, 61–70, 1991.

[barkley-1991]: https://homepages.warwick.ac.uk/staff/D.Barkley/Research/Papers/phys3d.pdf