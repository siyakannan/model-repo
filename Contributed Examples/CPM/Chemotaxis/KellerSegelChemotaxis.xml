<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>Keller-Segel chemotaxis </Title>
        <Details>Full title:		Chemotaxis
Authors:		L. Edelstein-Keshet
Contributors:	Y. Xiao
Date:		05.06.2022
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2010
File type:		Supplementary model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		A group of cells secrete and are attracted by a chemical, c. The chemical diffuses and decays in the domain. This model is a hybrid implementation of Keller-Segel Chemotaxis, e.g. in the paper: Keller, Evelyn F., Lee A. Segel. "Initiation of slime mold aggregation viewed as an instability." Journal of Theoretical Biology 26.3 (1970): 399-415.</Details>
    </Description>
    <Global>
        <Field symbol="c" name="attractant" value="0">
            <Diffusion rate="1"/>
            <BoundaryValue boundary="x" value="0"/>
            <BoundaryValue boundary="-x" value="0"/>
            <BoundaryValue boundary="y" value="0"/>
            <BoundaryValue boundary="-y" value="0"/>
            <Annotation>The chemical, c, diffuses, is produced by cells, and decays </Annotation>
        </Field>
        <System time-step="0.1" solver="Euler [fixed, O(1)]">
            <Constant symbol="k1" value="0.5"/>
            <Constant symbol="k2" value="0.05"/>
            <DiffEqn symbol-ref="c">
                <Expression>is_source*k1 - k2*c</Expression>
            </DiffEqn>
        </System>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="l" value="150, 150, 0"/>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="constant" boundary="-x"/>
                <Condition type="constant" boundary="y"/>
                <Condition type="constant" boundary="-y"/>
            </BoundaryConditions>
            <NodeLength value="0.1"/>
            <Neighborhood>
                <Distance>1.5</Distance>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="end" value="120"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="medium" name="medium">
            <Property symbol="max_c" value="0"/>
            <Property symbol="is_source" value="0"/>
        </CellType>
        <CellType class="biological" name="amoeba">
            <VolumeConstraint target="40" strength="1"/>
            <Chemotaxis name="attraction to higher c levels" strength="chi" field="c"/>
            <Property symbol="is_source" value="1">
                <Annotation>Cells are sources of the chemical and also undergo chemotaxis towards the chemical.</Annotation>
            </Property>
            <Property symbol="cs" name="Sensed chemical" value="0"/>
            <Function symbol="chi" name="Chemotaxis coefficient">
                <Expression>4</Expression>
            </Function>
            <Mapper name="sensing the chemical">
                <Input value="c"/>
                <Output symbol-ref="cs" mapping="average"/>
            </Mapper>
            <Annotation>The mapper takes values of the field and computes a cell property, namely the value of the chemical that the cells sense.</Annotation>
        </CellType>
    </CellTypes>
    <CPM>
        <Interaction>
            <Contact type1="amoeba" type2="amoeba" value="4"/>
            <Contact type1="amoeba" type2="medium" value="2"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration symbol="MCStime" value="0.001875"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <MetropolisKinetics yield="0.1" temperature="1.0"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Distance>1.5</Distance>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="amoeba" size="1">
            <InitCircle number-of-cells="20" mode="random">
                <Dimensions radius="l.x/2" center="l.x/2, l.y/2, 0"/>
            </InitCircle>
        </Population>
    </CellPopulations>>
    <Analysis>
        <Gnuplotter time-step="5" decorate="false">
            <Terminal name="png" size="400 400 0"/>
            <Plot>
                <Field symbol-ref="c" max="1.0" min="0">
                    <ColorMap>
                        <Color value="0" color="white"/>
                        <Color value="2.5" color="yellow"/>
                        <Color value="5.0" color="red"/>
                    </ColorMap>
                </Field>
                <Cells opacity="0.65" max="20" min="0.0" value="cell.id">
                    <ColorMap>
                        <Color value="1" color="red"/>
                        <Color value="20" color="blue"/>
                    </ColorMap>
                </Cells>
            </Plot>
        </Gnuplotter>
        <ModelGraph format="svg" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
