---
MorpheusModelID: M2000

title: Correlated Random Walkers

authors: [L. Edelstein-Keshet]
contributors: [D. Jahn]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1007/s00249-003-0300-4"
  title: "Analysis of actin dynamics at the leading edge of crawling cells: implications for the shape of keratocyte lamellipodia"
  authors: [H. P. Grimm, A. B. Verkhovsky, A. Mogilner, J.-J. Meister]
  journal: "Eur. Biophys. J."
  volume: 32
  #issue:
  page: "563–577"
  year: 2003
  #original_model: true

date: "2021-02-18T00:09:00+01:00"

tags:
- Actin
- Actin Dynamics
- Arp 2/3
- Arp2/3 Complex
- Asymmetric Cell Division
- Barbed End
- BoundaryConditions
- Branching
- CellDivision
- Filament
- Particle
- Periodic
- Periodic BoundaryConditions
- Trajectory

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
#- DOI:10.1007/s00249-003-0300-4
---
>Particles moving left and right (as well as up) to mimic actin filament barbed ends

## Introduction

Particles are moving left and right (as well as up) to mimic actin filament barbed ends as motivated by the [referenced paper](#reference).

<figure>
![](movie.mp4)
<figcaption>
Movie of the simulation showing the movement of particles representing actin filament barbed ends.
</figcaption>
</figure>

## Description

The simulation is initialized with a few cells close to <span title="//CellPopulations/Population[type='BarbedEnd']/InitRectangle[@mode='random' and @number-of-cells='5']/Dimensions[@size='200.0, 20.0, 0.0']">$`y = 0`$</span>, which are assigned random left or right directions.

Asymmetric <span title="//CellTypes/CellType[@name='BarbedEnd']/CellDivision[@orientation='1.0, 0.0, 0.0']">`CellDivision`</span> is used to create branches at some constant probability. One daughter goes left, one goes right.

The cell $`x`$ coordinate modulo the size of the $`x`$ range is also kept via a <span title="//CellTypes/CellType[@name='BarbedEnd']/System/Rule[@symbol-ref='xc']">`Rule`</span>, so that the cell trajectories ‘wrap around’ (so we see ‘the filaments’ of ends that wandered off the edge of the domain).

## Results

This simulation mimics the ‘unlimited Arp2/3 scenario’ where branching is not limited, and where branching takes place only close to the filament barbed ends.

![](plot_00800.png "Snapshot of the actual locations of the barbed ends with the ‘branch order’ (i.e. number of divisions) that they belong to assigned to each of them.")

The trajectory of the particles represents the actin filaments.

![](logger_plot_xc_cell.center.y_time_01400.png "‘Trails’ left by the particles, i.e. the branched structure of the actin filaments.")