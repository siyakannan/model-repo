<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Title>OneRandomWalkerWithChemicalBias</Title>
        <Details>Full title:		One Random Walker
Authors:		L. Edelstein-Keshet
Contributors:	Y. Xiao
Date:		10.05.2022
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2003
File type:		Supplementary model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		Cell attracted up a chemical gradient. As in OneRandomWalker_main.xml, but the frequency of the random changes in direction increases with the level of chemical sensed in the cell's environment. The cell starts at the center of the domain. A chemical gradient is created by a chemical (c) that diffuses from the left edge of the domain. The cell senses the average level of that chemical at its location (cs) and changes its direction of motion at increments of the run_time*(1+cs). The larger cs, the faster this happens. When the cell changes direction, its velocity vector is recomputed as a vector with random x and y components. As a result of this chemical-dependence, the cell tends to spend more time close to the higher chemical level.</Details>
    </Description>
    <Global>
        <Field symbol="c" name="Attractant" value="0.0">
            <Diffusion rate="0.2"/>
            <BoundaryValue boundary="x" value="5.0"/>
            <BoundaryValue boundary="-x" value="0.0"/>
            <Annotation>A diffusible chemical with constant value at the left domain edge.</Annotation>
        </Field>
    </Global>
    <Space>
        <Lattice class="square">
            <Size symbol="size" value="200, 200, 0"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="periodic" boundary="y"/>
                <Condition type="constant" boundary="-x"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="15000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="Walker">
            <VolumeConstraint target="200" strength="1"/>
            <ConnectivityConstraint/>
            <PropertyVector symbol="move_dir" value="0.0, 0.0, 0.0"/>
            <PropertyVector symbol="displacement" value="0.0, 0.0, 0.0"/>
            <PropertyVector symbol="velocity" value="0.0, 0.0, 0.0"/>
            <Property symbol="run_time" name="run duration" value="5.0"/>
            <Property symbol="last_turn_time" name="last random event" value="0"/>
            <Variable symbol="cs" value="0.0"/>
            <DirectedMotion direction="20*move_dir" strength="1"/>
            <Event trigger="when-true" time-step="5" name="Change direction">
                <Condition>time >= last_turn_time+ run_time/(1+cs)</Condition>
                <Rule symbol-ref="last_turn_time" name="Time of last random event">
                    <Expression>time</Expression>
                </Rule>
                <VectorRule symbol-ref="move_dir" name="new direction of motion">
                    <Expression>rand_uni(-1,1)/(1+cs), rand_uni(-1,1)/(1+cs) , 1</Expression>
                    <Annotation>When the condition is satisfied, chose a new random direction vector.</Annotation>
                </VectorRule>
            </Event>
            <Mapper time-step="1.0" name="sensed chemical">
                <Input value="c"/>
                <Output symbol-ref="cs" mapping="average"/>
                <Annotation>The mapper converts the chemical field c(x,y,t) into a value cs(t) that a given cell senses and reacts to.</Annotation>
            </Mapper>
            <MotilityReporter time-step="200">
                <Velocity symbol-ref="velocity"/>
            </MotilityReporter>
        </CellType>
        <CellType class="medium" name="medium"/>
    </CellTypes>
    <CPM>
        <Interaction>
            <Contact type1="Walker" type2="medium" value="4"/>
            <Contact type1="Walker" type2="Walker" value="10"/>
        </Interaction>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
            <MetropolisKinetics temperature="0.6"/>
        </MonteCarloSampler>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Distance>2.5</Distance>
            </Neighborhood>
        </ShapeSurface>
    </CPM>
    <CellPopulations>
        <Population type="Walker" size="1" name="Random Walker">
            <InitCircle number-of-cells="1" mode="random">
                <Dimensions radius="10.0" center="100.0, 100.0, 0.0"/>
            </InitCircle>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="200" decorate="false">
            <Terminal name="png"/>
            <Plot>
                <Cells value="cell.id"/>
                <!--    <Disabled>
        <CellArrows orientation="100*velocity"/>
    </Disabled>
-->
                <Field symbol-ref="c"/>
            </Plot>
        </Gnuplotter>
        <Logger time-step="10">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="5000">
                    <Style line-width="1.0" style="lines"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="cell.center.y"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="time"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <DisplacementTracker time-step="10" name="data" celltype="Walker"/>
        <ModelGraph format="svg" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
