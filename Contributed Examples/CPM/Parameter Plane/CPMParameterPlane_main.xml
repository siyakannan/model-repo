<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Full title:		CPM Parameter Plane
Authors:		L. Edelstein-Keshet
Contributors:	D. Jahn, Y. Xiao
Date:		26.02.2021
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2001
File type:		Main model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		Two-parameter plane showing all possible regimes of behaviour for  a single CPM cell. Idea is based on Fig. 3 in the followng paper: Magno, R., Grieneisen, V. A., Marée, A. F. (2015). The biophysical nature of cells: potential cell behaviours revealed by analytical and computational studies of cell surface mechanics. BMC biophysics, 8(1), 1-37. But here, we use two dimensionless parameters, dimensionless perimeter cost:  kappa=4*LambP/(LambA*Ra^2) – on the horizontal axis, dimensionless cell-medium contact energy cost:  beta= 2*Jmed/(3.14*LambA*Ra^3) – on the vertical axis, circularity: alpha = R_p/R_a – vary manually. Suggestion: try the settings alpha = 0.5, 1, 2. The results are now consistent with theoretical analysis in the above references. However, fine-tuning of the CPM settings was required to obtain the results. Simulation was fixed by Jörn Starruß (JS), and some comments added based on his help. JS: Morpheus 2.2 supports expressions in Contact energies directly, which may depend on properties of the given cell types. For example: &lt;Contact type1="Cell" type2="medium" value="cell1.Jmed">&lt;/Contact>. In previous versions of Morpheus, this had to be done by AddOn adhesion. JS: There is a constraint that prevents cells from vanishing from volume 1 to 0. You may use a CellDeath component to remove cells of volume 1. Usually we don't want cells to disappear due to shape constraint imbalances. JS: Creating a cell array with an initial shape works nicely with InitCellObjects: &lt;InitCellObjects mode="distance">&lt;Annotation>Initialize an 8x8 cell array  in a  square domain.&lt;/Annotation>&lt;Arrangement displacements="96, 96, 0" repetitions="8, 8, 1">&lt;Sphere radius="20.0" center="40.0, 40.0, 0.0"/>&lt;/Arrangement>&lt;/InitCellObjects>. JS: CPM dynamics have a lot of technical details in the CPM section. The simulation can get locked into local energy minima. JS increased the amplitude of fluctuations and smoothed the shape surface length approximation by choosing ShapeSurface/Neighborhood order 6 for the square lattice. Also choosing updates from Neighborhood order 2 is probably sufficient. This helped to get the theoretical results: MorpheusModel/CPM/MonteCarloSampler/MetropolisKinetics/temperature = 40, MorpheusModel/CPM/ShapeSurface/Neighborhood/Order = 6, MorpheusModel/CPM/MonteCarloSampler/Neighborhood/Order = 2.</Details>
        <Title>CPMParameterPlane</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Annotation>Set up the square grid</Annotation>
            <Neighborhood>
                <Order>4</Order>
            </Neighborhood>
            <Size symbol="size" value="750,750,0"/>
            <BoundaryConditions>
                <Condition type="noflux" boundary="x"/>
                <Condition type="noflux" boundary="y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="2000"/>
        <TimeSymbol symbol="time"/>
    </Time>
    <CellTypes>
        <CellType class="biological" name="Cell">
            <VolumeConstraint target="pi*(Ra^2)" strength="LambA"/>
            <SurfaceConstraint target="alpha*CircPerim" strength="LambP" mode="surface">
                <Annotation>JS: changed the surface constraint to an absolute value, not the relative-to-current-volume aspherity. LEK: alpha is still the same dimensionless ratio of radii</Annotation>
            </SurfaceConstraint>
            <Annotation>Initialize the cell with a volume and a perimeter constraint. The simulation will vary Jmed and LambP to get the distinct bergimes of behaviour. </Annotation>
            <Property symbol="Ra" name="Radius of rest area" value="20">
                <Annotation>Pick a convenient rest area radius so the cells will be visible.</Annotation>
            </Property>
            <Property symbol="LambP" value="cell.center.x/80"/>
            <Property symbol="LambA" value="10/(Ra^2)">
                <Annotation>To keep the "volume constraint" reasonable for larger radii, this parameter is scaled so that LambA*Ra^2=10 is fixed</Annotation>
            </Property>
            <Property symbol="Jmed" name="cell-medium contact energy term" value="6*(cell.center.y-250)"/>
            <Property symbol="alpha" name="Ratio of Rp to Ra" value="1">
                <Annotation>This is a dimensionless parameter that can be varied between 0 and something > 1. It is the ratio of the perimeter to the area rest radii</Annotation>
            </Property>
            <Property symbol="CircPerim" value="2*pi*Ra"/>
            <Property symbol="kappa" value="4*LambP/(LambA*Ra^2)">
                <Annotation>Dimensionless parameter varied for cells along the horizontal axis</Annotation>
            </Property>
            <Property symbol="beta" value="2*Jmed/(pi*LambA*Ra^3)">
                <Annotation>Dimensionless parameter varied for cells along the y axis</Annotation>
            </Property>
        </CellType>
        <CellType class="medium" name="medium"/>
    </CellTypes>
    <CPM>
        <Interaction>
            <Contact type1="Cell" type2="Cell" value="200"/>
            <Contact type1="Cell" type2="medium" value="cell1.Jmed"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>6</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="20"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
    <CellPopulations>
        <Population type="Cell" size="1">
            <InitCellObjects mode="distance">
                <Annotation>Initialize the cells in a regular square domain, so that the x and y coordinates of each cell will correspond to its CPm behaviour.</Annotation>
                <Arrangement displacements="96, 96, 0" repetitions="8, 8, 1">
                    <Sphere radius="20.0" center="40.0, 40.0, 0.0"/>
                </Arrangement>
            </InitCellObjects>
        </Population>
    </CellPopulations>
    <Analysis>
        <Gnuplotter time-step="100" decorate="true">
            <Plot>
                <Cells flooding="true" value="beta"/>
            </Plot>
            <Terminal name="png"/>
            <Plot>
                <Cells flooding="true" value="kappa"/>
            </Plot>
            <Annotation>The two plots are the same, but one is  colored by the beta value and the other by the kappa value. This also provides a scale for each.</Annotation>
        </Gnuplotter>
        <ModelGraph format="svg" reduced="false" include-tags="#untagged"/>
    </Analysis>
    <Global>
        <Constant symbol="NumCells" name="Number of cells to display" value="64"/>
        <Variable symbol="kappa" value="0.0"/>
        <Variable symbol="Jmed" value="0"/>
        <Variable symbol="beta" value="0.0"/>
    </Global>
</MorpheusModel>
