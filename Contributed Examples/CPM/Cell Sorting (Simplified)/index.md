---
MorpheusModelID: M2007

title: "Simplified Cell Sorting"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
publication:
  doi: "10.1103/PhysRevE.47.2128"
  title: "Simulation of the differential adhesion driven rearrangement of biological cells"
  authors: [J. A. Glazier, F. Graner]
  journal: "Phys. Rev. E"
  volume: 47
  issue: 3
  page: "2128"
  year: 1993
  #original_model: true

tags:
- Adhesion
- Adhesion Energy
- Cell-cell Contact
- Cell-medium Contact
- Contact
- Contact Energy
- Cell Sorting
- Cellular Potts Model
- Checkerboard
- CPM
- DAH
- Differential Adhesion
- Differential Adhesion Hypothesis
- Dispersal
- Engulfment
- Glazier-Graner-Hogeweg Model
- Glazier-Graner Model
- Interaction
- Medium
- MetropolisKinetics
- Reversal
- Separation
- Steinberg
- Target Area
- Target Volume
- Temperature
- Tissue Configuration
- VolumeConstraint

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> A simplified version of the Morpheus cell sorting example

## Introduction

A simplified version of the Morpheus built-in example of <a href="/model/m0021/" title="Morpheus Model ID: <code class='model-id-tooltip'>M0021</code>">cell sorting</a> showing the effect of cell-cell and cell-medium contact energies on the tissue configuration.

## Description

The different cell configurations were simulated with the parameters in the table attached, with b) being the preset behavior in {{< model_quick_access "media/model/m0007/CellSortingLEK.xml" >}}.

<figure>
<div class="d-flex justify-content-center">
<div>

| Type | <span title="//CPM/Interaction/Contact[@type1='ct1'][@type2='ct1']/@value">$`J_{11}`$</span> | <span title="//CPM/Interaction/Contact[@type1='ct2'][@type2='ct2']/@value">$`J_{22}`$</span> | <span title="//CPM/Interaction/Contact[@type1='ct1'][@type2='ct2']/@value">$`J_{12}`$</span> | <span title="//CPM/Interaction/Contact[@type1='ct1'][@type2='medium']/@value">$`J_{1 \mathrm m}`$</span> | <span title="//CPM/Interaction/Contact[@type1='ct2'][@type2='medium']/@value">$`J_{2 \mathrm m}`$</span> | <span title="//CPM/MonteCarloSampler/MetropolisKinetics/@temperature">$`T`$</span> | <span title="//CellTypes/CellType[name='ct1' or name='ct2']/VolumeConstraint/@strength">$`\lambda`$</span> |
|:---|---:|---:|---:|---:|---:|---:|---:|
| a) Checkerboard | $`10`$ | $`8`$ | $`6`$ | $`12`$ | $`12`$ | $`10`$ | $`1`$ |
| **b) Separation** | $`6`$ | $`6`$ | $`16`$ | $`12`$ | $`6`$ | $`2`$ | $`1`$ |
| c) Engulfment | $`14`$ | $`2`$ | $`11`$ | $`16`$ | $`16`$ | $`2`$ | $`1`$ |
| d) Reversal | $`14`$ | $`2`$ | $`11`$ | $`30`$ | $`16`$ | $`2`$ | $`1`$ |
| e) Dispersal 1 | $`14`$ | $`2`$ | $`9`$ | $`16`$ | $`16`$ | $`5`$ | $`1`$ |
| f) Dispersal 2 | $`14`$ | $`4`$ | $`11`$ | $`2`$ | $`16`$ | $`5`$ | $`1`$ |
</div>
</div>
<figcaption>Parameters as in <a href="#reference">Glazier <em>et al.</em>, 1993</a> used for the simulations</figcaption>
</figure>

## Results

![](CellSortingLEK.png "Regimes that can be found in a mixture of two cell types: a) checkerboard, b) separation (default in [`CellSortingLEK.xml`](#model)), c) engulfment, d) reversal, e) dispersal 1, f) dispersal 2.")

<figure>
  ![](M2007_cell-sorting-simplified_movie.mp4)
  <figcaption>
    Simulation video of <a href="#model"><code>CellSortingLEK.xml</code></a> with default behavior b)
  </figcaption>
</figure>