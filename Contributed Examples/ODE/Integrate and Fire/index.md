---
MorpheusModelID: M2004

title: "Integrate and Fire"

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- Discontinuous Output
- Excitable System
- Excitation
- Integrate-and-fire
- Integrate-and-fire Model
- Neuron
- Neuronal Excitation
- Neuronal Firing
- ODE
- Ordinary Differential Equation

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Simple model for neuronal firing

## Introduction

This is one of the simplest models used to depict neuronal firing.
## Description

A simple model for neuronal firing consists of the basic ‘integrate-and-fire’ system ODE:

$$\begin{align}
\frac{dV}{dt}=a_1, V=0 \text{ whenever } V \geq V_t
\end{align}$$

## Results

Here, the voltage builds up at a constant rate <span title="//Global/System/Constant[@symbol='a1']">$`a_1`$</span>. Once the voltage <span title="//Global/Variable[@symbol='Voltage']">$`V`$</span> reaches a set value (in this example, <span title="//Global/Variable[@symbol='Vt']">$`V_t = 10`$</span>), it is reset to zero, as shown in the following figure.

![](IntegrateAndFire.png "The simple integrate-and-fire model produces discontinuous output.")