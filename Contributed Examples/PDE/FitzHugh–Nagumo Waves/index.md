---
MorpheusModelID: M2014

title: FitzHugh–Nagumo Waves

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true
#  preprint: false

tags:
- 1D
- 2D
- Actin
- Actin Waves
- Asymmetry
- BoundaryConditions
- FitzHugh–Nagumo
- FitzHugh–Nagumo Wave
- Kymograph
- Partial Differential Equation
- PDE
- Periodic
- Periodic Boundary Conditions
- Periodic Domain
- Pulse
- Spiral
- Spiral Wave
- Traveling Pulse
- Traveling Wave
- Voltage
- Wave

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Waves produced by an excitable FitzHugh–Nagumo PDE.

## Introduction

We will here take a closer look at waves produced by an excitable FitzHugh–Nagumo PDE

1. in a periodic **1D domain**,
2. in a periodic **2D domain**.

## Description

Consider an excitable system such as the FitzHugh-Nagumo model that can produce traveling waves and pulses:

$$\begin{align}
\frac{\partial u}{\partial t} &= D\Delta u + \gamma u(1-u)(u-a) - v + I_0(x,y) \\\\
\frac{\partial v}{\partial t} &= \epsilon (u-bv) \\\\
\end{align}$$

## Results

Simulations of the model in 1D with periodic <span title="//Space/Lattice/BoundaryConditions">`BoundaryConditions`</span>, showing a traveling pulse (top) and in 2D, showing a set of spiral waves (bottom figures).

![](FN1Dwaves.png "The FitzHugh–Nagumo model in a periodic 1D domain produces a tavelling pulse. Left: kymograph of <span title='//Global/Field[@symbol=&#39;u&#39;]'>$`u`$</span> over time. Right: the shape of <span title='//Global/Field[@symbol=&#39;u&#39;]'>$`u`$</span> and <span title='//Global/Field[@symbol=&#39;v&#39;]'>$`v`$</span> for the pulse. Produced with [`FNwaves_main.xml`](#model).")

<figure>
  ![](M2014_fitzhugh-nagumo-waves_movie_FNwaves_main.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2014/FNwaves_main.xml" >}}
  </figcaption>
</figure>

<figure>
  ![](M2014_fitzhugh-nagumo-waves_movie_FN_PDE_in_2D.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2014/FN_PDE_in_2D.xml" >}} with Fitzhugh–Nagumo equations solved in 2D. There is current injected at the ‘origin’ to kickstart the waves. The initial profiles of <span title='//Global/Field[@symbol=&#39;u&#39;]'>$`u`$</span> (‘voltage’ or excitable variable) and <span title='//Global/Field[@symbol=&#39;v&#39;]'>$`v`$</span> (‘refractory’ variable) are asymmetric to provoke spirals.
  </figcaption>
</figure>

![](SpatialFNSpiralsNew.png "The FitzHugh–Nagumo PDEs can sustain spiral waves when stimulated in a 2D domain. Left: <span title='//Global/Field[@symbol=&#39;u&#39;]'>$`u`$</span>. Right: <span title='//Global/Field[@symbol=&#39;v&#39;]'>$`v`$</span> at $`t = 100`$. Produced with [`FNspiralwaves2D.xml`](#downloads).")

<figure>
    ![](M2014_fitzhugh-nagumo-waves_movie_FNspiralwaves2D.mp4)
    <figcaption>
        Simulation video of model {{< model_quick_access "media/model/m2014/FNspiralwaves2D.xml" >}}
    </figcaption>
</figure>
