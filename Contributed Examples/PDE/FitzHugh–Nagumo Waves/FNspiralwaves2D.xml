<MorpheusModel version="4">
    <Description>
        <Title>FNspiralwaves2D</Title>
        <Details>Full title:		FitzHugh–Nagumo Waves
Authors:		L. Edelstein-Keshet
Contributors:	Y. Xiao
Date:		23.06.2022
Software:		Morpheus (open-source). Download from https://morpheus.gitlab.io
Model ID:		https://identifiers.org/morpheus/M2014
File type:		Supplementary model
Reference:		L. Edelstein-Keshet: Mathematical Models in Cell Biology
Comment:		Waves produced by an excitable FitzHugh-Nagumo PDE in a periodic 2D domain. u is the "voltage", or excitable variable, v is the "refractory" variable. The initial conditions are a peak in u and a shallow ramp in both u and v (in y and x directions) so as to get asymmetry to get the spiral waves. The parameter values were suggested by Prof. James Keener, but with a larger domain size so as to see the spirals better. A hexagonal grid helps to prevent grid-effects.</Details>
    </Description>
    <Global>
        <Constant value="dx*size.x" symbol="L"/>
        <Function symbol="x">
            <Expression>dx*space.x</Expression>
        </Function>
        <Field value="0.5*y/L+0.4*exp(-((x-1)^2+(y-1)^2)/0.02)" name="u" symbol="u">
            <Diffusion rate="0.001"/>
        </Field>
        <Field value="0.1*x/L" name="v" symbol="v"/>
        <System solver="Dormand-Prince [adaptive, O(5)]">
            <DiffEqn symbol-ref="u">
                <Expression>H*(-u*(u-1)*(u-a)-v)</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="v">
                <Expression>eps*(u-b*v)</Expression>
            </DiffEqn>
            <Constant value="0.021" symbol="eps"/>
            <Constant value="2" symbol="b"/>
            <Constant value="0.1" symbol="a"/>
            <Constant value="20" symbol="H"/>
        </System>
        <Function symbol="y">
            <Expression>dx*space.y</Expression>
        </Function>
    </Global>
    <Space>
        <Lattice class="hexagonal">
            <Size value="200, 200,  0" symbol="size"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="periodic" boundary="y"/>
            </BoundaryConditions>
            <NodeLength value="0.04" symbol="dx"/>
            <Neighborhood>
                <Order>1</Order>
            </Neighborhood>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="200"/>
        <TimeSymbol name="time" symbol="t"/>
    </Time>
    <Analysis>
        <Logger time-step="5">
            <Input>
                <Symbol symbol-ref="u"/>
                <!--    <Disabled>
        <Symbol symbol-ref="v"/>
    </Disabled>
-->
                <Symbol symbol-ref="x"/>
                <Symbol symbol-ref="y"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="5">
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="x"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="y"/>
                    </Y-axis>
                    <Color-bar>
                        <Symbol symbol-ref="u"/>
                    </Color-bar>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph format="dot" reduced="false" include-tags="#untagged"/>
    </Analysis>
</MorpheusModel>
